This is the short term TODO list for the next release.
All long term TODO's should go on the [wiki](https://bitbucket.org/uiterlix/ravioli/wiki/Home).
Bugs that (for some strange reason) won't be fixed in the next release should go in the bug tracker.
This list should be empty when releasing a new version.

* just before release: make loadInContainerBundleDependencies the default again (or add startup option for setting the default)
* keep item selected when doing expand
* implement proper logging
* make hide an action on a component
* move unhide to selection table
* disable edge editing? (adding a label)
* terrible performance: hide all on large model (i.e. "select all", "make selection invisible")


