# Releases


### 0.2
**Release date: 22 Dec 2013**

New features:

* renamed from dependencyvisualise to Ravioli
* can now be used as stand-alone desktop application also (separate download)
* analyse bundle dependencies without having them deployed in an OSGi container, by reading bundle manifest files from file system
* added support for Require-Bundle and Fragment-Host bundle dependencies
* color of dependency arrows now depend on type of dependency
* export graph to file (PNG format)
* expand graph selection by adding nodes that have incoming dependencies
* toolbar buttons have now icons instead of textual labels
* various small UI enhancements, progress monitor for long during tasks
* performance improvements


### 0.1 
**Release date: 5 Dec 2013**

Features:

* shows bundle (package) dependencies for all bundles deployed in OSGi framework
* explains dependencies in terms of imported packages when hovering over a dependency 
* summarizes number of incoming and outgoing dependencies for each component
* supports various auto-layouts as well as manual layout
* components (and its dependencies) can be hidden to create a partial dependency graph
* expand function to unhide all components the current depends on, for step-by-step graph construction
* component nodes can be grouped to create a more concise overview
* view state (layout & grouping) can be saved to file (and loaded of course)
* zoom functions on graph view
* automatic renaming of component labels by means of regular expressions