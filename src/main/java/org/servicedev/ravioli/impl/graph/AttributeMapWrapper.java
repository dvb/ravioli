/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 * <p/>
 * This file is part of DependencyVisualise, an analysis and visualization tool
 * for OSGi components and their dependencies.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * <p/>
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph;

import org.jgraph.graph.AttributeMap;

/**
 * Wrapper class for org.jgraph.graph.AttributeMap to overcome an incompatability issue caused by the fact the
 * AttributeMap extends java.util.Hashtable in a non-generic way.
 * See https://groups.google.com/forum/#!topic/scala-user/dgHEASVc1ts
 */
public class AttributeMapWrapper {

    AttributeMap delegate;

    public AttributeMapWrapper() {
        delegate = new AttributeMap();
    }

    public AttributeMap get() {
        return delegate;
    }

    public void put(Object key, Object value) {
        delegate.put(key, value);
    }
}
