/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model

import scala.collection.mutable.ListBuffer
import scala.collection.JavaConversions._
import scala.collection.mutable.Map
import scala.collection.immutable.List

/**
 * The model that defines all components and their relationships. Outgoing relations between components can be queried
 * using <code>Component.getOutgoingDependencies</code>. Incoming relationships can be queried by <code>getIncomingDependencies(component : Component)</code>
 */
abstract trait ComponentModel {
  
  var incomingDependencies : Map[String, ListBuffer[Dependency]] = null

  /**
   * Returns all components in the model.
   * @return  all components
   */
  def getComponents(): java.util.List[Component]

  /**
   * Returns information about the creation of the model (e.g. invalid components), as an HTML-formatted string.
   * @return  HTML-formatted string or null.
   */
  def getCreationResult(): String = null

  /**
   * Explains a number of dependencies, for human consumption. The returned string should be HTML-formatted.
   * Note that the dependencies can have different source and/or target components, as the caller might be interested
   * in the dependencies between groups of components.
   *
   * @param dependencies  the dependencies to explain
   * @return  explanation or null if the model doesn't provide an explanation
   */
  def explainDependencies(dependencies: Seq[Dependency]): String = null
  
  /**
   * Returns the incoming dependencies for a component in the model. As a component is only aware of it's outgoing
   * dependencies, the model provides insight in the incoming dependencies.
   */
  def getIncomingDependencies(component : Component) : List[Dependency] = {
    if (incomingDependencies == null) {
      val dependencies = Map[String,ListBuffer[Dependency]]()
      for (component <- getComponents) {
        for (dependency <- component.getOutgoingDependencies) {
          val targetComponentIdentifier = dependency.getTargetComponent.getIdentifier
          val dependenciesForComponent = dependencies.get(targetComponentIdentifier)
          if (!dependenciesForComponent.isDefined) {
            dependencies(targetComponentIdentifier) = ListBuffer[Dependency]()
          }
          dependencies(targetComponentIdentifier) += dependency
        }
      }
      incomingDependencies = dependencies
    }
    incomingDependencies.getOrElse(component.getIdentifier, List[Dependency]()).toList
  }
  
  /**
   * Get property keys common to components in this component model
   */
  def getPropertyKeys() : Option[List[String]] = {
    None
  }
}