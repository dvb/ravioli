/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.dm;

import org.servicedev.ravioli.model.adapter.PresentationAdapter
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.Component

/**
/** 
 *  Presentation adapter dependency manager component models.
 */
 */
class DependencyManagerComponentModelPresentationAdapter extends PresentationAdapter {

  /**
   * Returns whether the presentation adapter is applicable for the given ComponentModel.
   */
  def isAdapterFor(componentModel : ComponentModel) : Boolean = {
    return componentModel.isInstanceOf[DependencyManagerComponentModel]
  }
  
  /**
   * Returns the presentation type for the given Dependency.
   */
  def getPresentationType(dependency : Dependency) : Option[String] = {
    if (dependency.isInstanceOf[DMImplementationDependency]) {
      return Some("implements")
    } else if (dependency.isInstanceOf[DMDependencyDependency]) {
      val serviceDependency = dependency.asInstanceOf[DMDependencyDependency]
      if (serviceDependency.getTargetComponent.isRequired) {
        if (serviceDependency.getTargetComponent.isAvailable) {
          return Some("required-dependency-available")
        } else {
          return Some("required-dependency-unavailable")
        }
      } else {
        if (serviceDependency.getTargetComponent.isAvailable) {
          return Some("optional-dependency-available")
        } else {
          return Some("optional-dependency-unavailable")
        }        
      }
    }
    None
  }
  
  /**
   * Returns the presentation type for the given Component.
   */
  def getPresentationType(component : Component) : Option[String] = {
    if (component.isInstanceOf[DMComponent]) {
      return Some("dm-component")
    } else if (component.isInstanceOf[ServiceReferenceComponent]) {
      return Some("osgi-service")
    }
    None
  }
}