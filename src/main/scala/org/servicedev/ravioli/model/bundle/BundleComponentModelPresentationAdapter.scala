package org.servicedev.ravioli.model.bundle

import org.servicedev.ravioli.model.adapter.PresentationAdapter
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.Component

/**
 * Presentation adapter for bundle component models.
 */
class BundleComponentModelPresentationAdapter extends PresentationAdapter {

  /**
   * Returns whether the presentation adapter is applicable for the given ComponentModel.
   */
  def isAdapterFor(componentModel : ComponentModel) : Boolean = {
    return componentModel.isInstanceOf[BundleComponentModel]
  }
  
  /**
   * Returns the presentation type for the given Dependency.
   */
  def getPresentationType(dependency : Dependency) : Option[String] = {
    if (dependency.isInstanceOf[BundleDependency]) {
      Some("bundle")
    } else if (dependency.isInstanceOf[PackageDependency]) {
      Some("package")
    } else if (dependency.isInstanceOf[FragmentHostDependency]) {
      Some("fragment-host")
    } else {
      None
    }
  }
  
  /**
   * Returns the presentation type for the given Component.
   */
  def getPresentationType(component : Component) : Option[String] = {
    None
  }
}