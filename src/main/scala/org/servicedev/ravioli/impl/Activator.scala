/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 * 
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl

import org.osgi.framework.BundleActivator
import org.servicedev.ravioli.impl.graph.GraphFrame
import org.osgi.framework.BundleContext
import javax.swing.SwingUtilities

class Activator extends BundleActivator {

  var app : GraphFrame = null

  def start(bundleContext: BundleContext) {
    app = new GraphFrame(bundleContext)
    app.setVisible(true)
  }

  def stop(bundleContext: BundleContext) {
    SwingUtilities.invokeLater(new Runnable() {
      def run() = {
        app.setVisible(false)
        app.dispose()
      }
    })
  }
}