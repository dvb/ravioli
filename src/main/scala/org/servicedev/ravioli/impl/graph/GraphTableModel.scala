/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import java.util.ArrayList
import java.util.List
import javax.swing.table.AbstractTableModel
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import org.servicedev.ravioli.impl.graph.model.GraphModel
import org.servicedev.ravioli.impl.graph.model.GraphModelChangeEvent
import org.servicedev.ravioli.impl.graph.model.GraphModelChangeListener
import java.lang
import scala.collection.mutable.Set
import scala.collection.JavaConversions._
import scala.collection.mutable.HashSet

class GraphTableModel(var graphModel: GraphModel) extends AbstractTableModel with GraphModelChangeListener {

  private val headers: Array[String] = Array[String]("On", "Component", "# out", "# in")
  // somehow classOf[Boolean] does not return the Boolean.class the table cell renderer can handle...
  private val booleanClass = new lang.Boolean(true).getClass
  private val stringClass = new lang.String().getClass
  private val integerClass = new lang.Integer(0).getClass

  private var cachedGraphComponents: List[GraphComponent] = new ArrayList[GraphComponent]
  // hashset for fast contains evaluation
  private var cachedGraphComponentSet: Set[GraphComponent] = new HashSet[GraphComponent]

  graphModel.addGraphModelChangeListener(this)
  cachedGraphComponents.addAll(graphModel.getGraphComponents)
  cachedGraphComponentSet ++= graphModel.getGraphComponents

  def setModel(graphModel: GraphModel) {
    graphModel.removeGraphModelChangeListener(this)
    this.graphModel = graphModel
    this.graphModel.addGraphModelChangeListener(this)
    reset
  }

  def reset {
    cachedGraphComponents.clear
    cachedGraphComponents.addAll(graphModel.getGraphComponents)
    cachedGraphComponentSet ++= graphModel.getGraphComponents
    fireTableDataChanged()
  }

  override def getColumnName(column: Int): String = {
    if (column < headers.length) {
      return headers(column)
    } else {
      // fetch from property keys of component model
      return graphModel.getComponentModel.getPropertyKeys.get.get(column - headers.length)
    }
  }

  def getColumnCount: Int = {
    var columnCount : Int = 0
    val propertyKeys = graphModel.getComponentModel.getPropertyKeys
    if (!propertyKeys.isDefined) {
      columnCount = headers.length
    } else {
      columnCount = headers.length + propertyKeys.get.length
    }
    columnCount
  }

  def getRowCount: Int = {
    return cachedGraphComponents.size
  }

  def getValueAt(row: Int, col: Int): Object = {
    val component: GraphComponent = cachedGraphComponents.get(row)
    if (col < headers.length) {
	    col match {
	      case 0 =>
	        return component.isVisible: java.lang.Boolean
	      case 1 =>
	        return component.getLabel
	      case 2 =>
	        return component.getOutgoingDependencies.size: java.lang.Integer
	      case 3 =>
	        return component.getIncomingDependencies.size: java.lang.Integer
	    }
    } else {
      // obtain from component properties
      val index = col - headers.length
      val propertyKey = graphModel.getComponentModel.getPropertyKeys.get(index)
      // also take into account that the property does not necessarily have to exist for composite graph components
      return component.getProperties.getOrElse(scala.collection.immutable.Map[String, Object]()).getOrElse(propertyKey, "")
    }
  }

  override def getColumnClass(col: Int): Class[_] = {
    if (col < headers.length) {
	    col match {
	      case 0 =>
	        return booleanClass
	      case 1 =>
	        return stringClass
	      case 2 =>
	        return integerClass
	      case 3 =>
	        return integerClass
	      case _ =>
	        throw new IllegalStateException("Unexpected column index.")
	    }
    } else {
      // obtain from component properties
      // val index = col - headers.length
      // val propertyKey = graphModel.getComponentModel.getPropertyKeys.get(index)
      // also take into account that the property does not necessarily have to exist for composite graph components
      return stringClass
    }
  }

  override def isCellEditable(rowIndex: Int, columnIndex: Int): Boolean = {
    val editable: Boolean = columnIndex == 0
    return editable
  }

  override def setValueAt(value: AnyRef, rowIndex: Int, columnIndex: Int) {
    super.setValueAt(value, rowIndex, columnIndex)
    if (columnIndex == 0) {
      val graphComponent: GraphComponent = cachedGraphComponents.get(rowIndex)
      graphComponent.setVisible(!graphComponent.isVisible)
    }
  }

  def getRow(component: GraphComponent): Int = {
    return cachedGraphComponents.indexOf(component)
  }

  def getGraphComponent(row: Int): GraphComponent = {
    return cachedGraphComponents.get(row)
  }

  def onGraphModelChange(event: GraphModelChangeEvent) {
    if (false) {
      setModel(graphModel)
    }
    else {
      fireTableDataChanged
      val graphComponentsToRemove: List[GraphComponent] = new ArrayList[GraphComponent]
      import scala.collection.JavaConversions._
      for (graphComponent <- cachedGraphComponents) {
        if (graphModel.getGraphComponent(graphComponent.getIdentifier) == null) {
          graphComponentsToRemove.add(graphComponent)
        }
      }
      import scala.collection.JavaConversions._
      for (graphComponent <- graphComponentsToRemove) {
        cachedGraphComponents.remove(graphComponent)
        cachedGraphComponentSet.remove(graphComponent)
      }
      import scala.collection.JavaConversions._
      for (graphComponent <- graphModel.getGraphComponents) {
        if (!cachedGraphComponentSet.contains(graphComponent)) {
          cachedGraphComponents.add(graphComponent)
          cachedGraphComponentSet.add(graphComponent)
        }
      }
      refresh
    }
  }

  def refresh {
    fireTableDataChanged
  }

}