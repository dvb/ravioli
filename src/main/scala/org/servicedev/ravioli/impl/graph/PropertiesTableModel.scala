/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import javax.swing.table.AbstractTableModel
import org.servicedev.ravioli.impl.graph.model.GraphModelChangeListener
import org.servicedev.ravioli.model.Component
import scala.collection.mutable.LinkedHashMap
import scala.collection.mutable.ListBuffer

/**
 * TableModel for the properties of the selected component.
 */
class PropertiesTableModel extends AbstractTableModel {

  private val headers: Array[String] = Array[String]("Key", "Value")
  private var selectedComponentPropertyKeys = ListBuffer[String]()
  private var selectedComponentProperties = Map[String, Object]()
  
  def setSelectedComponent(component : Component) = {
    updateComponentProperties(component)
    fireTableDataChanged()
  }
  
  private def updateComponentProperties(selectedComponent : Component) = {
    if (selectedComponent != null) {
      selectedComponentProperties = selectedComponent.getProperties.getOrElse(Map[String, Object]())
	  selectedComponentPropertyKeys.clear
      selectedComponentProperties.foreach { case (key, value) =>
        selectedComponentPropertyKeys += key
      }
    }
  }
  
  override def getColumnName(column: Int): String = {
    return headers(column)
  }

  def getColumnCount: Int = {
    return 2
  }

  def getRowCount: Int = {
    selectedComponentProperties.size
  }
  
  def getValueAt(row: Int, col: Int): Object = {
    val propertyKey = selectedComponentPropertyKeys.apply(row)
    col match {
      case 0 =>
        return propertyKey
      case 1 =>
        return selectedComponentProperties(propertyKey)
      case _ =>
        throw new IllegalStateException("Unexpected column index.")
    }
  }

  override def getColumnClass(col: Int): Class[_] = {
    col match {
      case 0 =>
        return classOf[String]
      case 1 =>
        return classOf[Object]
      case _ =>
        throw new IllegalStateException("Unexpected column index.")
    }
  }

}