/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import javax.swing.JButton
import java.awt.event.ActionListener
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import javax.swing.ImageIcon
import org.jgraph.JGraph
import java.awt.event.ActionEvent
import scala.collection.mutable.ListBuffer
import scala.collection.JavaConversions._
import javax.swing.BorderFactory
import java.awt.Dimension
import org.servicedev.ravioli.impl.graph.model.GraphModel
import javax.swing.JSplitPane
import javax.swing.JTable
import javax.swing.JScrollPane
import org.osgi.framework.BundleContext
import scala.util.Try
import org.servicedev.ravioli.Constants
import org.servicedev.ravioli.model.NullModel
import org.servicedev.ravioli.model.base.DefaultComponentModel
import javax.swing.JFrame
import javax.swing.JMenuBar
import scala.collection.immutable.List
import javax.swing.JMenuItem
import scala.collection.mutable.ListBuffer
import javax.swing.JMenu
import javax.swing.JCheckBoxMenuItem
import org.servicedev.ravioli.Launcher
import java.awt.BorderLayout
import javax.swing.JToolBar
import javax.swing.WindowConstants
import javax.swing.JPanel
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import org.jgraph.event.GraphSelectionListener
import org.jgraph.event.GraphSelectionEvent
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent
import java.awt.event.MouseEvent
import org.jgraph.graph.DefaultEdge
import org.servicedev.ravioli.impl.graph.model.GraphDependency
import javax.swing.ToolTipManager
import java.awt.Color
import javax.swing.JFileChooser
import org.servicedev.ravioli.model.ComponentModel
import javax.swing.SwingWorker
import javax.swing.JDialog
import javax.swing.JProgressBar
import javax.swing.JLabel
import javax.swing.SwingUtilities
import org.servicedev.ravioli.impl.graph.model.GraphModel
import scala.util.control.Breaks._
import javax.swing.BoxLayout
import java.util.concurrent.ExecutionException
import java.util.zip.ZipException
import javax.swing.JOptionPane
import org.servicedev.ravioli.model.bundle.DirectoryScanningManifestComponentModel
import java.io.File
import java.awt.Cursor
import javax.swing.filechooser.FileNameExtensionFilter
import java.io.IOException
import javax.swing.JTextPane
import javax.swing.UIManager
import org.servicedev.ravioli.model.bundle.InContainerBundleScanner
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import com.jgraph.layout.JGraphLayout
import com.jgraph.layout.JGraphFacade
import com.jgraph.layout.organic.JGraphFastOrganicLayout
import com.jgraph.layout.graph.JGraphSimpleLayout
import com.jgraph.layout.simple.SimpleGridLayout
import java.util.ArrayList
import java.util.Collections
import org.servicedev.ravioli.impl.graph.model.CompositeGraphComponent
import org.jgraph.graph.DefaultGraphCell
import org.servicedev.ravioli.model.jar.PackageComponentModel
import org.servicedev.ravioli.model.jar.JarFileScanner
import org.servicedev.ravioli.model.jar.ClassFileScanner
import org.servicedev.ravioli.model.dm.DependencyManagerComponentModel
import org.servicedev.ravioli.impl.graph.model.SimpleGraphComponent
import javax.swing.JViewport

class GraphFrame extends JFrame {
  val FRAME_WIDTH = 1024
  val FRAME_HEIGHT = 768
  val buttonSize = new Dimension(40, 28)
  var dividerSize : Int = 0
  var model: GraphModel = new GraphModel(new NullModel(), null)
  var standalone = true
  var jGraphModelAdapter: JGraphModelAdapter = null
  var jGraph: JGraph = null
  val morpher = new JGraphLayoutMorphingManager()
  var splitPane: JSplitPane = null
  var selectionTable: JTable = null
  var selectionTableModel: GraphTableModel = null
  var graphScrollPane: JScrollPane = null
  var sideBarVisible = true
  var bundleContext: BundleContext = null
  var labelConverter = new RegexBasedLabelConverter()
  val propertiesTableModel = new PropertiesTableModel()
  val debug = Try(System.getProperty(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY).toBoolean).getOrElse(false)

  {
    labelConverter.setRegexReplacements(LabelPrefsDialog.getLabelReplacementPrefs);
    initializeFrame(() => {})
  }

  def this(bundleContext : BundleContext) = {
    this()
    this.standalone = false
    this.bundleContext = bundleContext
    this.model = new GraphModel(new DefaultComponentModel(), labelConverter)
//    initializeFrame(() => loadInContainerBundleDependencies())
    initializeFrame(() => loadDependencyManagerDependencies())
  }

  def initializeFrame(postInitializeAction: () => Unit) = {
    createUI()
    postInitializeAction()
  }

  private def createUI() = {
    setSize(FRAME_WIDTH, FRAME_HEIGHT)
    setJMenuBar(createMenuBar)
    add(createToolBar, BorderLayout.NORTH)
    setTitle(Launcher.getExtendedName)
    setLocationRelativeTo(null)
    var closeOperation = WindowConstants.DISPOSE_ON_CLOSE
    if (standalone) closeOperation = JFrame.EXIT_ON_CLOSE
    setDefaultCloseOperation(closeOperation)
    splitPane = new JSplitPane()
    getContentPane().add(splitPane)
    val graphPanel = createGraphPanel()
    val selectionPanel = createSelectionTablePanel()
    val propertiesPanel = createPropertiesTablePanel()
    splitPane.setLeftComponent(graphPanel)
    val tableSplitPane = new JSplitPane()
    tableSplitPane.setTopComponent(selectionPanel)
    tableSplitPane.setBottomComponent(propertiesPanel)
    tableSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT)
    splitPane.setRightComponent(tableSplitPane)
    splitPane.addComponentListener(new ComponentAdapter() {
      override def componentResized(componentEvent : ComponentEvent) = {
    	  if (sideBarVisible) {
    		  splitPane.setDividerLocation(computeDividerLocation())
    	  }
      }
    })
    jGraph.addGraphSelectionListener(new GraphSelectionListener() {
      def valueChanged(e : GraphSelectionEvent) = {
        handleGraphSelectionChanged()
      }
    })
    selectionTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      def valueChanged(e : ListSelectionEvent) = {
        handleTableSelectionChanged()
      }
    })
    splitPane.setDividerLocation(computeDividerLocation())
    tableSplitPane.setDividerLocation(500)
  }

  private def createGraphPanel() : JScrollPane = {
    jGraphModelAdapter = new JGraphModelAdapter(model)
    jGraph = new JGraph(jGraphModelAdapter) {
      override def getToolTipText(e : MouseEvent) : String = {
        val cell = jGraph.getFirstCellForLocation(e.getX(), e.getY())
        if (cell.isInstanceOf[DefaultEdge]) {
          val userObject = cell.asInstanceOf[DefaultEdge].getUserObject()
          userObject.asInstanceOf[GraphDependency].getExplanation()
        }
        else {
          null
        }
      }
    }
    ToolTipManager.sharedInstance().registerComponent(jGraph)

    graphScrollPane = new JScrollPane(jGraph)
    graphScrollPane
  }

  private def createSelectionTablePanel(): JScrollPane = {
    selectionTableModel = new GraphTableModel(model)
    selectionTable = new JTable(selectionTableModel)
    selectionTable.setAutoCreateRowSorter(true)
    selectionTable.setGridColor(Color.BLACK);
    updateSelectionTableColumnModel()
    val scrollPane = new JScrollPane(selectionTable)
    selectionTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS)
    scrollPane.setPreferredSize(new Dimension(227, 600))
    scrollPane
  }
  
  private def updateSelectionTableColumnModel() = {
    selectionTableModel.fireTableStructureChanged()
	val columnModel = selectionTable.getColumnModel()
    columnModel.getColumn(0).setPreferredWidth(20)
    columnModel.getColumn(0).setMaxWidth(30)
    columnModel.getColumn(1).setPreferredWidth(120)
    columnModel.getColumn(2).setPreferredWidth(40)
    columnModel.getColumn(2).setMaxWidth(50)
    columnModel.getColumn(3).setPreferredWidth(30)
    columnModel.getColumn(3).setMaxWidth(50)  
    // give other columns a preferred width
    val columnCount = columnModel.getColumnCount()
    if (columnCount > 4) {
	    for (index <- 4 to (columnCount - 1)) {
	      columnModel.getColumn(index).setPreferredWidth(50)
	    }
    }
  }
  
  private def createPropertiesTablePanel() : JScrollPane = {
    val propertiesTable = new JTable(propertiesTableModel)
    propertiesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
    val scrollPane = new JScrollPane(propertiesTable)
    scrollPane.setPreferredSize(new Dimension(227, 150))
    val columnModel = propertiesTable.getColumnModel()
    columnModel.getColumn(0).setPreferredWidth(80);
    columnModel.getColumn(1).setPreferredWidth(147);
    propertiesTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS)
    scrollPane
  }

  private def createMenuBar() : JMenuBar = {
    val menuBar = new JMenuBar()
    val fileMenu = new JMenu("File")
    fileMenu.add(createJMenuItem("(Re)Scan OSGi Framework", () => { bundleContext != null }, () => { loadInContainerBundleDependencies() }))
    fileMenu.add(createJMenuItem("(Re)Scan DM Components", () => { bundleContext != null }, () => { loadDependencyManagerDependencies() }))
    fileMenu.add(createJMenuItem("Scan bundle manifests...", () => { true }, () => { doScanBundleManifests() }))
    fileMenu.add(createJMenuItem("Save view state...", () => { true }, () => { saveViewState() }))
    fileMenu.add(createJMenuItem("Load view state...", () => { true }, () => { loadViewState() }))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Export to PNG...", () => { true }, () => { exportToPNG() }))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Exit", () => { true }, () => { exit() }))
    menuBar.add(fileMenu)

    val editMenu = new JMenu("Edit")
    editMenu.add(createJMenuItem("Select all", null, () => { selectAll() } ))
    editMenu.add(createJMenuItem("Unselect all", null, () => { selectNone() } ))
    menuBar.add(editMenu)

    val viewMenu = new JMenu("View")
    viewMenu.add(createJCheckBoxMenuItem("Toggle sidebar", () => { sideBarVisible }, () => { toggleSideBar() }))
    if (debug) {
    	viewMenu.add(createJCheckBoxMenuItem("Refresh", () => { true }, () => { refreshModels() }))
    }
    menuBar.add(viewMenu)
    val preferencesMenu = new JMenu("Preferences")
    preferencesMenu.add(createJMenuItem("Labels...", () => { true }, () => { showPreferencesDialog() }))
    menuBar.add(preferencesMenu)
    val helpMenu = new JMenu("Help")
    helpMenu.add(createJMenuItem("About " + Launcher.getName() + "...", () => { true }, () => { showAbout() }))
    menuBar.add(helpMenu)
    menuBar
  }

  private def createToolBar() : JToolBar = {
    val toolBar = new JToolBar()
    toolBar.add(createJButton("Exit", "/icons/system-log-out-6.png", { () => exit() }))
    toolBar.add(createJButton("Force directed layout", "/icons/office-chart-polar.png", { () => applyForceDirectedLayout() }))
    toolBar.add(createJButton("Circle layout", "/icons/office-chart-ring.png", { () => applyCircleLayout() }))
    toolBar.add(createJButton("Grid layout", "/icons/distribute-horizontal-margin.png", { () => applyGridLayout() }))
    toolBar.add(createJButton("Default zoom", "/icons/zoom-original-2.png", { () => jGraph.setScale(1.0) }))
    toolBar.add(createJButton("Zoom in", "/icons/zoom-in-3.png", { () => jGraph.setScale(jGraph.getScale() * 2) }))
    toolBar.add(createJButton("Zoom out", "/icons/zoom-out-3.png", { () => jGraph.setScale(jGraph.getScale() / 2) }))
    toolBar.add(createJButton("Group selection", "/icons/format-join-node.png", { () => groupCurrentSelection() }))
    toolBar.add(createJButton("Unroup selection", "/icons/format-break-node.png", { () => unGroupCurrentSelection() }))
    toolBar.add(createJButton("Make selection visible", "/icons/format-add-node.png", { () => makeSelectionVisible() }))
    toolBar.add(createJButton("Make selection invisible", "/icons/format-remove-node.png", { () => makeSelectionInvisible() }))
    toolBar.add(createJButton("Expand outgoing dependencies", "/icons/go-next-9.png", { () => expandOutgoingDependenciesForSelection() }))
    toolBar.add(createJButton("Expand incoming dependencies", "/icons/go-previous-9.png", { () => expandIncomingDependenciesForSelection() }))
    if (debug) {
      toolBar.add(createJButton("Debug", null, { () => debugSelection() }))
    }
    toolBar
  }

  private def doScanBundleManifests() = {
    val manifestDir = selectDirectory()
    if (manifestDir.isDefined) {
      loadManifestBundleDependencies(manifestDir.get)
    }
  }

  private def selectDirectory() : Option[File] = {
    val fileSelector = new JFileChooser()
    fileSelector.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
    if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
        Some(fileSelector.getSelectedFile())
    } else {
    	None
    }
  }

  def loadManifestBundleDependencies(manifestDir : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new DirectoryScanningManifestComponentModel(manifestDir, this)
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  def loadModel(componentModel : ComponentModel) = {
    new LoadTask() {
      override def getComponentModel() = {
        componentModel
      }
      override def isComponentsDefaultVisible() = {
        false
      }
    }.execute()
  }

  def loadJarFilePackageDependencies(jarFile : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new PackageComponentModel(new JarFileScanner(jarFile, this))
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  def loadClassFilePackageDependencies(classesDir : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new PackageComponentModel(new ClassFileScanner(classesDir, this))
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  private def saveViewState() = {
    val fileSelector = new JFileChooser();
    if (JFileChooser.APPROVE_OPTION == fileSelector.showSaveDialog(this)) {
      model.save(fileSelector.getSelectedFile())
    }
  }

  private def loadViewState() = {
    val fileSelector = new JFileChooser()
    if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR))
      val start = System.currentTimeMillis()
      model.load(fileSelector.getSelectedFile())
      selectionTableModel.reset
      setCursor(Cursor.getDefaultCursor())
      if (debug) {
        println("Loading '" + fileSelector.getSelectedFile() + "' took " + (System.currentTimeMillis() - start) + " ms.")
      }
    }
  }

  private def exportToPNG() = {
    val fileSelector = new JFileChooser()
    val filter = new FileNameExtensionFilter("PNG format", "png")
    fileSelector.setFileFilter(filter)
    if (JFileChooser.APPROVE_OPTION == fileSelector.showSaveDialog(this)) {
      val imageFile = fileSelector.getSelectedFile()
      try {
        ImageIO.write(jGraph.getImage(null, 10), "png", imageFile)
      } catch {
        case ex: IOException => JOptionPane.showMessageDialog(GraphFrame.this, "Saving image file failed.", "Error", JOptionPane.ERROR_MESSAGE)
        case ex: OutOfMemoryError => JOptionPane.showMessageDialog(GraphFrame.this, "Creating image failed due to lack of memory; please restart your JVM with larger heap size.", "Error", JOptionPane.ERROR_MESSAGE)
      }
    }
  }

  private def exit() = {
    if (standalone) {
      System.exit(0)
    } else {
      GraphFrame.this.setVisible(false)
      GraphFrame.this.dispose()
    }
  }

  private def toggleSideBar() = {
    if (sideBarVisible) {
      dividerSize = splitPane.getDividerSize()
      splitPane.setDividerSize(0)
      splitPane.setDividerLocation(1.0)
      splitPane.getRightComponent().setVisible(false)
    }
    else {
      splitPane.setDividerSize(dividerSize)
      splitPane.setDividerLocation(computeDividerLocation())
      splitPane.getRightComponent().setVisible(true)
    }
    sideBarVisible = !sideBarVisible
  }

  private def refreshModels() = {
    jGraphModelAdapter.refresh
    selectionTableModel.refresh
  }

  private def showAbout() = {
    val programName = Launcher.getName()

    val fontFamily = GraphFrame.this.getFont().getFamily()
    val aboutText = "<html><body style=\"font-family: " + fontFamily + "\"><h3>" + programName + " " + Launcher.getVersion() + "</h3>" +
            "<p>" + programName + " is open source and licensed under LGPL.<br>" +
            "Included libraries:<br>" +
            Launcher.getLibs().mkString("<br>") +
            "</p>" +
            "<p style=\"padding-top:10px;\">For more info see " + Launcher.getInfoUrl() + "<p>" +
            "</html>"
    val textPane = new JTextPane()
    val backgroundColor = UIManager.getColor("OptionPane.background")
    textPane.setBackground(backgroundColor)
    textPane.setEditable(false)
    textPane.setContentType("text/html")
    textPane.setText(aboutText)

    JOptionPane.showMessageDialog(this, textPane, "About " + programName, JOptionPane.INFORMATION_MESSAGE)
  }

  private def showPreferencesDialog() = {
    val dlg = new LabelPrefsDialog(this)
    dlg.setLocationRelativeTo(this)
    dlg.setVisible(true)
    labelConverter.setRegexReplacements(dlg.getReplacements)
    jGraphModelAdapter.refreshNodes
  }

  private def loadInContainerBundleDependencies() = {
    new LoadTask() {
      override def getComponentModel() = {
        new InContainerBundleScanner(bundleContext, this)
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }
  
  private def loadDependencyManagerDependencies() = {
    new LoadTask() {
      override def getComponentModel() = {
        new DependencyManagerComponentModel(bundleContext)
      }
      override def isComponentsDefaultVisible() = {
        false
      }      
    }.execute()
  }

  private def debugSelection() = {
    for (component <- getSelectedComponents()) {
      println("Component " + component + ":")
      println("Out:")
	  for (dep <- component.getOutgoingDependencies()) {
	    println("- " + dep.getTargetComponent() + " (" + dep.getExplanation() + ")")
	  }
	  println("In:")
	  for (dep <- component.getIncomingDependencies()) {
	    println("- " + dep.getSourceComponent() + " (" + dep.getExplanation() + ")")
	  }
    }
  }

  private def getSelectedComponents() : List[GraphComponent] = {
    val selectedRows = selectionTable.getSelectedRows
    val selectedComponents = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      selectedComponents += selectionTableModel.getGraphComponent(modelRowIndex)
    }
    selectedComponents.toList
  }

  private def applyForceDirectedLayout() = {
    val layout = new JGraphFastOrganicLayout()
    layout.setForceConstant(200)
    applyAutoLayout(layout)
  }

  private def applyCircleLayout() = {
    val layout = new JGraphSimpleLayout(JGraphSimpleLayout.TYPE_CIRCLE, 1024, 768)
    applyAutoLayout(layout)
  }

  private def applyGridLayout() = {
    val layout = new SimpleGridLayout()
    applyAutoLayout(layout)
  }

  private def applyAutoLayout(layout: JGraphLayout) = {
    val graphFacade = new JGraphFacade(jGraph)
    layout.run(graphFacade)
    val nestedMap = graphFacade.createNestedMap(true, true)
    morpher.morph(jGraph, nestedMap)
  }

  private def groupCurrentSelection() = {
    val messageType = JOptionPane.INFORMATION_MESSAGE
    val compositeIdentifier = JOptionPane.showInputDialog(GraphFrame.this,
       "Composite identifier?",
       "Create composite component", messageType)
    if (compositeIdentifier != null && !compositeIdentifier.trim().equals("")) {  // Value is null when dialog is cancelled
        if (!model.containsIdentifier(compositeIdentifier)) {
    	  doGroupCurrentSelection(compositeIdentifier)
        } else {
          JOptionPane.showConfirmDialog(GraphFrame.this, "Identifier '" + compositeIdentifier + "' is already used in this model.", "Error", JOptionPane.DEFAULT_OPTION)
        }
    }
    else if (compositeIdentifier != null) {
      JOptionPane.showConfirmDialog(GraphFrame.this, "Group identifier can't be empty.", "Error", JOptionPane.DEFAULT_OPTION)
    }
  }

  private def doGroupCurrentSelection(groupIdentifier: String) = {
    // Gather the selected graph components
    val selectedRows = selectionTable.getSelectedRows()
    val selectedComponents = new ArrayList[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex)
      if (!graphComponent.isInstanceOf[CompositeGraphComponent]) {
        selectedComponents.add(graphComponent)
      }
    }
    // And group them
    if (selectedComponents.size() > 0) {
      // TODO (discuss): creating groups of size 1 does not make much sense, does it?
      val groupedComponent = model.group(selectedComponents, groupIdentifier);
      // select the group
      val cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(Collections.singletonList(groupedComponent))
      jGraph.setSelectionCells(cellsForGraphComponents.toArray())
    }
  }


  private def unGroupCurrentSelection() = {
    val selectedRows = selectionTable.getSelectedRows()
    val selectedComponents = new ArrayList[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex)
      selectedComponents.add(graphComponent)
    }
    // ungroup selected components
    val ungroupedComponents = new ArrayList[GraphComponent]()
    for (graphComponent <- selectedComponents) {
      if (graphComponent.isInstanceOf[CompositeGraphComponent]) {
        ungroupedComponents.addAll(model.ungroup(graphComponent.asInstanceOf[CompositeGraphComponent]))
      }
    }
    // select the components that are ungrouped
    val cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(ungroupedComponents)
    jGraph.setSelectionCells(cellsForGraphComponents.toArray())
  }

  private def selectAll() {
    selectionTable.selectAll()
  }

  private def selectNone() {
    selectionTable.clearSelection()
  }

  private def makeSelectionVisible() = {
    val selectedRows = selectionTable.getSelectedRows();
    val componentsToUnhide = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex)
      componentsToUnhide += graphComponent
    }
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
  }

  private def makeSelectionInvisible() = {
    val selectedRows = selectionTable.getSelectedRows();
    val componentsToHide = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex)
      componentsToHide += graphComponent
    }
    model.changeComponentsVisibility(componentsToHide.toList, false)
  }

  private def expandOutgoingDependenciesForSelection() = {
    val selectedRows = selectionTable.getSelectedRows();
    val componentsToUnhide = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
      for (dependency <- graphComponent.getOutgoingDependencies()) {
        componentsToUnhide += dependency.getTargetComponent
      }
    }
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
  }

  private def expandIncomingDependenciesForSelection() = {
    val selectedRows = selectionTable.getSelectedRows();
    val componentsToUnhide = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex);
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex);
      for (dependency <- graphComponent.getIncomingDependencies()) {
        componentsToUnhide += dependency.getSourceComponent
      }
    }
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
  }

  private def computeDividerLocation() : Int = {
    val rightSize = splitPane.getRightComponent().getPreferredSize().width
    var width = splitPane.getWidth()
    // before the frame becomes visible the initial width returns 0
    if (width == 0) {
      width = FRAME_WIDTH;
    }
    width - rightSize
  }

  private def handleGraphSelectionChanged() = {
    val selectedComponents = new ArrayList[GraphComponent]()
    val currentSelectedRows = selectionTable.getSelectedRows()
    // convert to Integer[] for easy comparison
    val currentSelectedRowList = new ArrayList[Integer]()
    for (rowIndex <- currentSelectedRows) {
      currentSelectedRowList.add(rowIndex);
    }
    val newSelectedRows = new ArrayList[Integer]();
    breakable {
      for (cell <- jGraph.getSelectionCells()) {
        if (cell.isInstanceOf[DefaultGraphCell]) {
          val graphCell = cell.asInstanceOf[DefaultGraphCell];
          val userObject = graphCell.getUserObject()
          if (userObject.isInstanceOf[GraphComponent]) {
            selectedComponents.add(userObject.asInstanceOf[GraphComponent]);
            val modelRowIndex = selectionTableModel.getRow(userObject.asInstanceOf[GraphComponent])
            try {
              newSelectedRows.add(selectionTable.convertRowIndexToView(modelRowIndex))
            } catch {
              case e: IndexOutOfBoundsException => {
                // table model is out of sync TODO: Find a better way to handle this
                selectedComponents.clear();
                break;
              }
            }
          }
        }
      }
    }
    if (!currentSelectedRowList.equals(newSelectedRows)) {
      val selectionModel = selectionTable.getSelectionModel()
      selectionModel.clearSelection();
      for (rowIndex <- newSelectedRows) {
        selectionModel.addSelectionInterval(rowIndex, rowIndex)
      }
    }
    val viewport = selectionTable.getParent().asInstanceOf[JViewport];
    if (selectionTable.getSelectedRow() > -1) {
	    val rect = selectionTable.getCellRect(selectionTable.getSelectedRow(), -1, true);
	    val insets = selectionTable.getInsets();
	    rect.x = insets.left;
	    rect.width = selectionTable.getWidth() - insets.left - insets.right;
	    selectionTable.scrollRectToVisible(rect);    
    }  
    propagateComponentSelectionChange()
  }

  private def handleTableSelectionChanged() = {
    val selectedRows = selectionTable.getSelectedRows()
    val selectedComponents = new ArrayList[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = selectionTableModel.getGraphComponent(modelRowIndex)
      selectedComponents.add(graphComponent)
    }
    val cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(selectedComponents)
    jGraph.setSelectionCells(cellsForGraphComponents.toArray())
    if (cellsForGraphComponents.size() > 0) {
      val firstSelectedCell = cellsForGraphComponents.get(0)
      jGraph.scrollCellToVisible(firstSelectedCell)
    }
    propagateComponentSelectionChange()
  }
  
  def propagateComponentSelectionChange() = {
    val selectedComponents = getSelectedComponents
    if (selectedComponents.size > 0) {
      // update properties table selected component
      val selectedComponent = selectedComponents.get(0)
      if (selectedComponent.isInstanceOf[SimpleGraphComponent]) {
    	propertiesTableModel.setSelectedComponent(selectedComponent.asInstanceOf[SimpleGraphComponent].getComponent)
      } else {
        // clear selection
        propertiesTableModel.setSelectedComponent(null)
      }
    } else {
      propertiesTableModel.setSelectedComponent(null)
    }
  }

  def createJMenuItem(title : String, condition : () => Boolean, callback: () => Unit) : JMenuItem = {
    val menuItem = new JMenuItem(title)
    if (condition != null)
      menuItem.setEnabled(condition())
    menuItem.addActionListener(createActionListener(callback))
    menuItem
  }

  def createJCheckBoxMenuItem(title : String, condition : () => Boolean, callback: () => Unit) : JMenuItem = {
    val menuItem = new JCheckBoxMenuItem(title, condition())
    menuItem.addActionListener(createActionListener(callback))
    menuItem
  }


  def createJButton(tooltip: String, iconLocation: String, callback: () => Unit): JButton = {
    val button = new JButton()
    if (iconLocation != null) {
	    val buttonIcon = ImageIO.read(this.getClass().getResourceAsStream(iconLocation))
	    button.setIcon(new ImageIcon(buttonIcon))
    } else {
      button.setText(tooltip)
    }
    button.setToolTipText(tooltip)
    button.setMinimumSize(buttonSize)
    button.setMaximumSize(buttonSize)
    button.setPreferredSize(buttonSize)
    button.addActionListener(createActionListener(callback))
    button
  }

  def createActionListener(callback: () => Unit): ActionListener = {
    new ActionListener() {
      def actionPerformed(event: ActionEvent) = {
        callback()
      }
    }
  }

  abstract class LoadTask() extends SwingWorker[GraphModel, String] with Progress {
    var start : Long = 0
    var end : Long = 0
    var progressCount : Int = 0

    var taskTitle : String = null
    var progressDlg : JDialog = null
    val progressBar = new JProgressBar()
    val progressText = new JLabel()

    {
	  this.taskTitle = "Creating model"
	  progressBar.setIndeterminate(true)
	  setProgressText("Starting...")
    }

    override def setRange(min : Int, max : Int) = {
      SwingUtilities.invokeLater(new Runnable() {
        def run() = {
          progressBar.setIndeterminate(false)
          progressBar.setMinimum(min)
          progressBar.setMaximum(max)
        }
      })
    }

    override def increment() = {
      progressCount = progressCount + 1
      publish(" ")
    }

    override def setCurrentProgress(progress : Int) = {
      progressCount = progress
      publish(" ")
    }

    override def setProgressText(message : String) = {
      publish(message)
    }

    override def setIndeterminate(on : Boolean) = {
      progressBar.setIndeterminate(on)
    }

    override def cancelled() : Boolean = {
      isCancelled()
    }

    def getComponentModel() : ComponentModel
    
    def isComponentsDefaultVisible() : Boolean

    override def doInBackground() : GraphModel = {
      start = System.currentTimeMillis()
      val newModel = new GraphModel(getComponentModel(), labelConverter, isComponentsDefaultVisible)
      setProgressText("Preparing model...")
      newModel.prepare()
      setProgressText("Updating graph...")
      newModel
    }

    override def process(messages : java.util.List[String]) = {
      // Only show progress after some time
      if ((System.currentTimeMillis() - start) > 100) {
        if (progressDlg == null) {
          // This might seem strange, as this method is called on the EDT, but when invoke directly, the
          // call will block on the setVisible of the modal dialog and this call will thus not return. In
          // some cases (when the worker finishes very fast), this will lead to a progress dialog that
          // does not close when done.
          // Strange enough, this does not happen always (as you would expect); it seems the EDT is handling
          // long blocking modal dialogs in a 'smarter' way...
          SwingUtilities.invokeLater(new Runnable() {
            def run() = {
              showProgressDialog()
            }
          })
        }
        progressBar.setValue(progressCount)
        // Find latest (non-empty) message
        breakable {
          if (messages.size > 0) {
	          for (i <- messages.size to 1 by -1) {
	            val current = messages.get(i - 1)
	            if (current != null && current.length() > 0) {
	        	  progressText.setText(current);
	              break
	            }
	          }
          }
        } // end breakable
      }
    }

    private def showProgressDialog() = {
      if (!isDone()) {
        progressDlg = new JDialog(GraphFrame.this, taskTitle, true)
        progressDlg.setLocationRelativeTo(GraphFrame.this)

        val panel = new JPanel()
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20))
        val boxLayout = new BoxLayout(panel, BoxLayout.PAGE_AXIS)
        panel.setLayout(boxLayout)

        panel.add(progressBar)
        progressText.setPreferredSize(new Dimension(200, progressText.getPreferredSize().height))
        progressText.setAlignmentX(0.5f)
        panel.add(progressText)

        val cancelButton = new JButton("cancel")
        cancelButton.setAlignmentX(0.5f)
        cancelButton.addActionListener(new ActionListener() {
            def actionPerformed(actionEvent : ActionEvent) {
                cancel(true)
            }
        });
        panel.add(cancelButton)

        progressDlg.add(panel)
        progressDlg.pack()
        progressDlg.setVisible(true)
      }
    }

    override def done() = {
      if (debug) println("creating model took " + (System.currentTimeMillis() - start) +  " ms")  // TODO: use logger
      val startUI = System.currentTimeMillis()
      if (! isCancelled()) {
        try {
          model = get()
          val models = System.currentTimeMillis()
          jGraphModelAdapter = new JGraphModelAdapter(model)
          if (debug) println("creating JGraphModel took " + (System.currentTimeMillis()-models) + " ms")
          jGraph.setModel(jGraphModelAdapter)
          graphScrollPane.setViewportView(jGraph)
          selectionTableModel.setModel(model)
          updateSelectionTableColumnModel
          selectionTableModel.refresh
          applyForceDirectedLayout()
        } catch {
          case ex : InterruptedException => {
            // (thrown by SwingWorker.get()) - Impossible: we're done
          }
          case ex: ExecutionException => {
            if (ex.getCause().isInstanceOf[ZipException]) {
              JOptionPane.showMessageDialog(GraphFrame.this, "Invalid jar file.", "Error", JOptionPane.ERROR_MESSAGE)
            } else {
              if (debug) {
                println("An error occurred during loading: " + ex);
                ex.printStackTrace()
              }
              JOptionPane.showMessageDialog(GraphFrame.this, "A surprising new error has occurred.", "Error", JOptionPane.ERROR_MESSAGE)
            }
          }
        } // end catch
      }
      end = System.currentTimeMillis()
      if (progressDlg != null && end - start < 500) {
        // Avoid flashing, display progress dialog a little longer
        try {
          Thread.sleep(500)
        } catch {
          case ex: InterruptedException => {}
        }
      }

      if (progressDlg != null) {
        progressDlg.setVisible(false)
        progressDlg.dispose()
      }

      val endUI = System.currentTimeMillis();
      if (debug) {
        println("creating UI took " + (endUI - startUI) + " ms")
        println("Loading took " + (end-start) + " ms")
      }

      val result = model.getComponentModel().getCreationResult()
      if (result != null && result.trim().length() > 0) {
        JOptionPane.showMessageDialog(GraphFrame.this, result, "result", JOptionPane.OK_OPTION)
      }

    } // end of done()
  } // end of LoadTask

}
